import React from 'react';
import ReactDOM from 'react-dom';
/*bootstrap*/
import 'bootstrap/dist/css/bootstrap.min.css';

/*components*/
import AppNavbar from './components/AppNavbar';
import Banner from './components/Banner';
import Highlights from './components/Highlights';
import CourseCard from './components/CourseCard';



ReactDOM.render(
  <div>
    <AppNavbar /> 
    <Banner />
    <Highlights />
    <CourseCard />
  </div>,  
  document.getElementById('root')
);


