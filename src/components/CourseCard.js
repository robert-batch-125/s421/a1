import React from 'react';

/*react-bootstrap components*/
import {
	Row,
	Col,
	Card,
	Button	
} from 'react-bootstrap';

export default function CourseCard(){

	return(

		<Row className="px-3 ">
			<Col xs={12}>
				<Card>
				  <Card.Body>
				    <Card.Title>Sample Course</Card.Title>
				    <Card.Subtitle>Description</Card.Subtitle>
				    <Card.Text>This is a single course offering.</Card.Text>
				    <Card.Subtitle>Price:</Card.Subtitle>
				    <Card.Text>PHP 40,000</Card.Text>
				    <Button variant="primary">Enroll</Button>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}

